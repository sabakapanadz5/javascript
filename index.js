//  პირველი დავალება
let A = false;
let B = true;
let C = false;
let D;
let E;
let F = true;
let G = 3;
let H = 10;
let I = -3;
let J = 10;

console.log(A && (B || !D)); // false
console.log(B && (!A || D) != !F); // true
console.log(C || ((!B || !F) && (A || E))); // false
console.log((A && B == F) || ((D || F) && !C)); // true
console.log(C || F == !A || (E == D) == !B || F); // true
console.log(!C && J == H); //true
console.log(A || !B || H > I); // true
console.log((B != C || I > G) != (!A && B)); // true
console.log(!A || (J == C && (E || !C == !A))); // true
console.log(A || (H > I && J >= H != B) || A); //false

//  მეორე დავალება

// პირველი ნაწილი

let neoTaskFirst = 60;
let neoTaskSecond = 57;
let neoTaskThird = 72;
let neoTaskFourth = 88;
let neoSum = neoTaskFirst + neoTaskSecond + neoTaskThird + neoTaskFourth;

let indianaTaskFirst = 78;
let indianaTaskSecond = 52;
let indianaTaskThird = 66;
let indianaTaskFourth = 80;
let indianaSum =
  indianaTaskFirst + indianaTaskSecond + indianaTaskThird + indianaTaskFourth;

let severusoTaskFirst = 75;
let severusoTaskSecond = 67;
let severusoTaskThird = 54;
let severusoTaskFourth = 90;
let severusSum =
  severusoTaskFirst +
  severusoTaskSecond +
  severusoTaskThird +
  severusoTaskFourth;

let aladinTaskFirst = 80;
let aladinTaskSecond = 52;
let aladinTaskThird = 68;
let aladinTaskFourth = 76;
let aladinSum =
  aladinTaskFirst + aladinTaskSecond + aladinTaskThird + aladinTaskFourth;

console.log();
console.log("Indiana has " + indianaSum + " score");
console.log("Severus has " + severusSum + " score");
console.log("Neo has " + neoSum + " score");
console.log("Aladin has " + aladinSum + " score");

let bestScore;
let bestStudent;
// if
if (neoSum >= indianaSum && neoSum >= severusSum && neoSum >= aladinSum) {
  bestScore = neoSum;
} else if (
  indianaSum >= neoSum &&
  indianaSum >= severusSum &&
  indianaSum >= aladinSum
) {
  bestScore = indianaSum;
} else if (
  severusSum >= neoSum &&
  severusSum >= indianaSum &&
  severusSum >= aladinSum
) {
  bestScore = severusSum;
} else if (
  aladinSum >= neoSum &&
  aladinSum >= indianaSum &&
  aladinSum >= severusSum
) {
  bestScore = aladinSum;
}
//switch
// switch (true) {
//   case neoSum >= indianaSum && neoSum >= severusSum && neoSum >= aladinSum:
//     bestScore = neoSum;
//     break;
//   case indianaSum >= neoSum &&
//     indianaSum >= severusSum &&
//     indianaSum >= aladinSum:
//     bestScore = indianaSum;
//     break;
//   case severusSum >= neoSum &&
//     severusSum >= indianaSum &&
//     severusSum >= aladinSum:
//     bestScore = severusSum;
//     break;
//   case aladinSum >= neoSum &&
//     aladinSum >= indianaSum &&
//     aladinSum >= severusSum:
//     bestScore = aladinSum;
//     break;
// }
// if
if (bestScore == neoSum) {
  bestStudent = "Neo";
} else if (bestScore == indianaSum) {
  bestStudent = "Indiana";
} else if (bestScore == severusSum) {
  bestStudent = "Severusa";
} else if (bestScore == aladinSum) {
  bestStudent = "Aladini";
}
// switch
// switch (bestScore) {
//   case neoSum:
//     bestStudent = "Neo";
//     break;
//   case indianaSum:
//     bestStudent = "Indiana";
//     break;
//   case severusSum:
//     bestStudent = "Severusa";
//     break;
//   case aladinSum:
//     bestStudent = "Aladini";
//     break;
// }

console.log(bestStudent + " got best score: " + bestScore);

// მეორე ნაწილი

let neoArithmeticAverage = neoSum / 4;
let indianaArithmeticAverage = indianaSum / 4;
let severusArithmeticAverage = severusSum / 4;
let aladinArithmeticAverage = aladinSum / 4;

console.log();
console.log(neoArithmeticAverage);
console.log(indianaArithmeticAverage);
console.log(severusArithmeticAverage);
console.log(aladinArithmeticAverage);

let bestArithmeticAverage;
let bestArithmeticAverageStudent;
// if
if (
  neoArithmeticAverage >= indianaArithmeticAverage &&
  neoArithmeticAverage >= severusArithmeticAverage &&
  neoArithmeticAverage >= aladinArithmeticAverage
) {
  bestArithmeticAverage = neoArithmeticAverage;
} else if (
  indianaArithmeticAverage >= neoArithmeticAverage &&
  indianaArithmeticAverage >= severusArithmeticAverage &&
  indianaArithmeticAverage >= aladinArithmeticAverage
) {
  bestArithmeticAverage = indianaArithmeticAverage;
} else if (
  severusArithmeticAverage >= neoArithmeticAverage &&
  severusArithmeticAverage >= indianaArithmeticAverage &&
  severusArithmeticAverage >= aladinArithmeticAverage
) {
  bestArithmeticAverage = severusArithmeticAverage;
} else if (
  aladinArithmeticAverage >= neoArithmeticAverage &&
  aladinArithmeticAverage >= indianaArithmeticAverage &&
  aladinArithmeticAverage >= severusArithmeticAverage
) {
  bestArithmeticAverage = aladinArithmeticAverage;
}

switch (true) {
  case neoArithmeticAverage >= indianaArithmeticAverage &&
    neoArithmeticAverage >= severusArithmeticAverage &&
    neoArithmeticAverage >= aladinArithmeticAverage:
    bestArithmeticAverage = neoArithmeticAverage;
    break;
  case indianaArithmeticAverage >= neoArithmeticAverage &&
    indianaArithmeticAverage >= severusArithmeticAverage &&
    indianaArithmeticAverage >= aladinArithmeticAverage:
    bestArithmeticAverage = indianaArithmeticAverage;
    break;
  case severusArithmeticAverage >= neoArithmeticAverage &&
    severusArithmeticAverage >= indianaArithmeticAverage &&
    severusArithmeticAverage >= aladinArithmeticAverage:
    bestArithmeticAverage = severusArithmeticAverage;
    break;
  case aladinArithmeticAverage >= neoArithmeticAverage &&
    aladinArithmeticAverage >= indianaArithmeticAverage &&
    aladinArithmeticAverage >= severusArithmeticAverage:
    bestArithmeticAverage = aladinArithmeticAverage;
    break;
}

if (bestArithmeticAverage == neoArithmeticAverage) {
  bestArithmeticAverageStudent = "Neo";
} else if (bestArithmeticAverage == indianaArithmeticAverage) {
  bestArithmeticAverageStudent = "Indiana";
} else if (bestArithmeticAverage == severusArithmeticAverage) {
  bestArithmeticAverageStudent = "Severusa";
} else if (bestArithmeticAverage == aladinArithmeticAverage) {
  bestArithmeticAverageStudent = "Aladini";
}

switch (bestArithmeticAverage) {
  case neoArithmeticAverage:
    bestArithmeticAverageStudent = "Neo";
    break;
  case indianaArithmeticAverage:
    bestArithmeticAverageStudent = "Indiana";
    break;
  case severusArithmeticAverage:
    bestArithmeticAverageStudent = "Severusa";
    break;
  case aladinArithmeticAverage:
    bestArithmeticAverageStudent = "Aladini";
    break;
}

console.log(
  bestArithmeticAverageStudent +
    " got best arithmetic average : " +
    bestArithmeticAverage
);

// მესამე ნაწილი

let neoArithmeticAveragePercentage = (neoArithmeticAverage * 100) / 100;
let indianaArithmeticAveragePercentage = (indianaArithmeticAverage * 100) / 100;
let severusArithmeticAveragePercentage = (severusArithmeticAverage * 100) / 100;
let aladinArithmeticAveragePercentage = (aladinArithmeticAverage * 100) / 100;

console.log();
console.log(neoArithmeticAveragePercentage + "%");
console.log(indianaArithmeticAveragePercentage + "%");
console.log(severusArithmeticAveragePercentage + "%");
console.log(aladinArithmeticAveragePercentage + "%");

let bestArithmeticAveragePercentage;
let bestArithmeticAverageStudentPercentage;
if (
  neoArithmeticAveragePercentage >= indianaArithmeticAveragePercentage &&
  neoArithmeticAveragePercentage >= severusArithmeticAveragePercentage &&
  neoArithmeticAveragePercentage >= aladinArithmeticAveragePercentage
) {
  bestArithmeticAverageParcentage = neoArithmeticAveragePercentage;
} else if (
  indianaArithmeticAveragePercentage >= neoArithmeticAveragePercentage &&
  indianaArithmeticAveragePercentage >= severusArithmeticAveragePercentage &&
  indianaArithmeticAveragePercentage >= aladinArithmeticAveragePercentage
) {
  bestArithmeticAverageParcentage = indianaArithmeticAveragePercentage;
} else if (
  severusArithmeticAveragePercentage >= neoArithmeticAveragePercentage &&
  severusArithmeticAveragePercentage >= indianaArithmeticAveragePercentage &&
  severusArithmeticAveragePercentage >= aladinArithmeticAveragePercentage
) {
  bestArithmeticAverageParcentage = severusArithmeticAveragePercentage;
} else if (
  aladinArithmeticAveragePercentage >= neoArithmeticAveragePercentage &&
  aladinArithmeticAveragePercentage >= indianaArithmeticAveragePercentage &&
  aladinArithmeticAveragePercentage >= severusArithmeticAveragePercentage
) {
  bestArithmeticAverageParcentage = aladinArithmeticAveragePercentage;
}

switch (true) {
  case neoArithmeticAveragePercentage >= indianaArithmeticAveragePercentage &&
    neoArithmeticAveragePercentage >= severusArithmeticAveragePercentage &&
    neoArithmeticAveragePercentage >= aladinArithmeticAveragePercentage:
    bestArithmeticAverageParcentage = neoArithmeticAveragePercentage;
    break;
  case indianaArithmeticAveragePercentage >= neoArithmeticAveragePercentage &&
    indianaArithmeticAveragePercentage >= severusArithmeticAveragePercentage &&
    indianaArithmeticAveragePercentage >= aladinArithmeticAveragePercentage:
    bestArithmeticAverageParcentage = indianaArithmeticAveragePercentage;
    break;
  case severusArithmeticAveragePercentage >= neoArithmeticAveragePercentage &&
    severusArithmeticAveragePercentage >= indianaArithmeticAveragePercentage &&
    severusArithmeticAveragePercentage >= aladinArithmeticAveragePercentage:
    bestArithmeticAverageParcentage = severusArithmeticAveragePercentage;
    break;
  case aladinArithmeticAveragePercentage >= neoArithmeticAveragePercentage &&
    aladinArithmeticAveragePercentage >= indianaArithmeticAveragePercentage &&
    aladinArithmeticAveragePercentage >= severusArithmeticAveragePercentage:
    bestArithmeticAverageParcentage = aladinArithmeticAveragePercentage;
}

if (bestArithmeticAverageParcentage == neoArithmeticAveragePercentage) {
  bestArithmeticAverageStudent = "Neo";
} else if (
  bestArithmeticAverageParcentage == indianaArithmeticAveragePercentage
) {
  bestArithmeticAverageStudent = "Indiana";
} else if (
  bestArithmeticAverageParcentage == severusArithmeticAveragePercentage
) {
  bestArithmeticAverageStudent = "Severusa";
} else if (bestArithmeticAverageParcentage == aladinArithmeticAverage) {
  bestArithmeticAverageStudent = "Aladini";
}

switch (bestArithmeticAverageParcentage) {
  case neoArithmeticAveragePercentage:
    bestArithmeticAverageStudent = "Neo";
    break;
  case indianaArithmeticAveragePercentage:
    bestArithmeticAverageStudent = "Indiana";
    break;
  case severusArithmeticAveragePercentage:
    bestArithmeticAverageStudent = "Severusa";
    break;
  case aladinArithmeticAverage:
    bestArithmeticAverageStudent = "Aladini";
    break;
}

console.log(
  bestArithmeticAverageStudent +
    " got best percentage : " +
    bestArithmeticAverageParcentage +
    "%"
);

// gpa-ს გამოთვლა

let firstTask = 4;
let secondTask = 2;
let thirdTask = 7;
let fourthTask = 5;
let creditSum = firstTask + secondTask + thirdTask + fourthTask;
// neo 05 *2
// neo 1 * 4
// neo 7 *6
// neo 7 *9
let neoTaskFirstGpa = 0.5;
let neoTaskSecondGpa = 0.5;
let neoTaskThirdGpa = 2;
let neoTaskFourthGpa = 3;

let indianaTaskFirstGpa = 2;
let indianaTaskSecondGpa = 0.5;
let indianaTaskThirdGpa = 1;
let indianaTaskFourthGpa = 2;

let severusoTaskFirstGpa = 2;
let severusoTaskSecondGpa = 1;
let severusoTaskThirdGpa = 0.5;
let severusoTaskFourthGpa = 3;

let aladinTaskFirstGpa = 2;
let aladinTaskSecondGpa = 0.5;
let aladinTaskThirdGpa = 1;
let aladinTaskFourthGpa = 2;

let neoGpa =
  (neoTaskFirstGpa * firstTask +
    neoTaskSecondGpa * secondTask +
    neoTaskThirdGpa * thirdTask +
    neoTaskFourthGpa * fourthTask) /
  creditSum;
console.log(neoGpa);
let indianaGpa =
  (indianaTaskFirstGpa * firstTask +
    indianaTaskSecondGpa * secondTask +
    indianaTaskThirdGpa * thirdTask +
    indianaTaskFourthGpa * fourthTask) /
  creditSum;
console.log(indianaGpa);
let severusoGpa =
  (severusoTaskFirstGpa * firstTask +
    severusoTaskSecondGpa * secondTask +
    severusoTaskThirdGpa * thirdTask +
    severusoTaskFourthGpa * fourthTask) /
  creditSum;
console.log(severusoGpa);
let aladinGpa =
  (aladinTaskFirstGpa * firstTask +
    aladinTaskSecondGpa * secondTask +
    aladinTaskThirdGpa * thirdTask +
    aladinTaskFourthGpa * fourthTask) /
  creditSum;
console.log(aladinGpa);
